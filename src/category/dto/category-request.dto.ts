import { IsOptional, IsNotEmpty } from 'class-validator';

export class CategoryRequest {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  slug: string;

  @IsOptional()
  description: string;
}
