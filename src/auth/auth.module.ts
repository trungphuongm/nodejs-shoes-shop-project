import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { EncryptionModule } from '../common/services/encryption.module';
import { TokenModule } from '../common/services/token.module';
import { EmailModule } from '../common/services/email.module';
import { OtpModule } from '../common/services/otp.module';

@Module({
  imports: [EncryptionModule, TokenModule, EmailModule, OtpModule],
  providers: [AuthService],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
