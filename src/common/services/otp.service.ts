import * as speakeasy from 'speakeasy';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OtpService {
  async createForgotPasswordToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      const emailToken = speakeasy.totp({
        secret: process.env.BASE32,
        encoding: 'base32',
        window: 2880,
      });
      if (emailToken) return resolve(emailToken);
      else return reject();
    });
  }

  async verifyForgotPasswordToken(token: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const forgotToken = speakeasy.totp.verify({
        secret: process.env.BASE32,
        encoding: 'base32',
        token,
        window: 2880,
      });
      if (forgotToken) return resolve(forgotToken);
      else return reject();
    });
  }
}
