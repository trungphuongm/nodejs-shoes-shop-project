INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (1, 'home','home', null, '1.', null);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (2, 'category1','category-1', null, '1.2', 1);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (3, 'category2','category-2', null, '1.3', 1);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (4, 'category3','category-3', null, '1.4', 1);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (5, 'Coporate Shoes','coporate-shoes', null, '1.2.5', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (6, 'Sneakers','sneakers', null, '1.2.6', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (7, 'Sandals','sandals', null, '1.2.7', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (14, 'Sport Shoes','sport-shoes', null, '1.2.14', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (15, 'Trainers','trainers', null, '1.2.15', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (16, 'Coporate Shoes','coporate-shoes', null, '1.2.16', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (26, 'Sneakers','sneakers', null, '1.2.26', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (27, 'Sandals','sandals', null, '1.2.27', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (28, 'Sport Shoes','sport-shoes', null, '1.2.28', 2);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (29, 'Trainers','trainers', null, '1.2.29', 2);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (8, 'HOT DEAL','hot-deal', null, '1.3.8', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (9, 'Sunglasses','sunglasses', null, '1.3.9', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (10, 'Belts','belts', null, '1.3.10', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (17, 'Handbags','handbags', null, '1.3.17', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (18, 'Sneakers','sneakers', null, '1.3.18', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (21, 'HOT DEAL','hot-deal', null, '1.3.21', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (22, 'Sunglasses','sunglasses', null, '1.3.22', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (23, 'Belts','belts', null, '1.3.23', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (24, 'Handbags','handbags', null, '1.3.24', 3);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (25, 'Sneakers','sneakers', null, '1.3.25', 3);

INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (11, 'Coporate Shoes','coporate-shoes', null, '1.4.11', 4);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (12, 'Sneakers','sneakers', null, '1.4.12', 4);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (13, 'Sandals','sandals', null, '1.4.13', 4);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (19, 'Sport Shoes','sport-shoes', null, '1.4.19', 4);
INSERT INTO public.category("id", "name", "slug", "description", "mpath", "parentCategoryId")
VALUES (20, 'Trainers','trainers', null, '1.4.20', 4);