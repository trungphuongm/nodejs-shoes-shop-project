import { IsOptional, IsEmail, Validate, IsIn } from 'class-validator';
import { IsValidMobile } from '../../common/validators/is-valid-mobile';
import {
  GENDER_FEMALE,
  GENDER_MALE,
  GENDER_OTHER,
} from '../../common/constant';

export class UserUpdate {
  @IsOptional()
  firstName: string;

  @IsOptional()
  lastName: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @Validate(IsValidMobile)
  mobile: string;

  @IsOptional()
  @IsIn([GENDER_MALE, GENDER_FEMALE, GENDER_OTHER])
  gender: number;

  @IsOptional()
  birthday: Date;

  @IsOptional()
  jti: string;
}
