import { User } from './user.entity';

export interface SearchPaginationResponse {
  data: User[];
  totalCount: number;
}

export interface CreateUserResponse {
  id: string;
}
