/** SQL insert first time
* ------------------------ username / password = admin@gmail.com / abcd1234
*/
INSERT INTO public.role("name", "access") VALUES ('Quản trị hệ thống', '*');

INSERT INTO public.user("email", "password", "salt", "firstName", "lastName", "mobile", "status", "roleId","birthday")
VALUES (
  'admin1@gmail.com',
  '486ccd30c01bd861f79797bacdf23fd53bb93cfa8f3ce4ccd4be46232c34d3b5fe54ce85876687b161556deace02451ff6e6760c6b14f3400490b67d2d4491e3',
  '8fac13aa69',
  'admin',
  'system',
  '0374539633',
  'ACTIVATED',
  1,
now()
);
