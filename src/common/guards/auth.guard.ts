import {
  Injectable,
  CanActivate,
  ExecutionContext,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { TokenService } from '../services/token.service';
import { UserService } from '../../user/user.service';
import {
  ACCOUNT_ACTIVE,
  ERROR_LOGIN_OTHER_DEVICE,
  ERROR_STATUS_USER_CHANGED,
} from '../constant';
import { RoleService } from '../../role/role.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
    private readonly roleService: RoleService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const { authorization } = request.headers;
    if (!authorization) {
      throw new BadRequestException('Authorization header is required');
    }

    try {
      const account: any = await this.tokenService.verifyToken(authorization);
      console.log(account, ' account');
      const { jti, status, roleId } = await this.userService.findOne(
        account.id,
      );
      if (status !== ACCOUNT_ACTIVE) {
        throw new BadRequestException(ERROR_STATUS_USER_CHANGED);
      }
      if (jti !== account.jti) {
        throw new BadRequestException(ERROR_LOGIN_OTHER_DEVICE);
      }
      const role = await this.roleService.findOne(roleId);
      if (!role) {
        throw new NotFoundException('Cannot find Role');
      }
      request.account = account;
      request.account.access = role.access;

      return true;
    } catch (error) {
      throw error;
    }
  }
}
