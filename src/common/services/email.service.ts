import { Injectable, InternalServerErrorException } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class EmailService {
  private readonly transporter = null;

  constructor() {
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      },
    });
  }

  async sendEmail(data: any): Promise<any> {
    const fields = {
      from: process.env.SMTP_FROM,
      to: [data.email],
      cc: [],
      bcc: [],
      subject: 'Shoes Shop',
      text: 'Hello',
      html: `<a href="${data.link}">Click here</a> for reset password`,
      attachments: [],
    };
    const message = Object.assign(fields, data);
    return this.transporter.sendMail(message).catch((error) => {
      throw new InternalServerErrorException(
        `Can't send to email cause: ${error.message}`,
      );
    });
  }
}
