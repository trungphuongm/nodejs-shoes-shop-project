import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { EncryptionService } from '../common/services/encryption.service';
import { TokenService } from '../common/services/token.service';
import {
  ForgotPasswordResponse,
  RegisterResponse,
  TokenResponse,
  VerifyForgotPasswordResponse,
} from './auth.interface';
import { OtpService } from '../common/services/otp.service';
import { UserService } from '../user/user.service';
import { RegisterRequest } from './dto';
import { EmailService } from '../common/services/email.service';
@Injectable()
export class AuthService {
  constructor(
    private readonly encryptionService: EncryptionService,
    private readonly tokenService: TokenService,
    private readonly userService: UserService,
    private readonly emailService: EmailService,
    private readonly otpService: OtpService,
  ) {}

  async forgotPassword({ email }): Promise<ForgotPasswordResponse> {
    const user = await this.userService.findOne({ email });
    if (!user) {
      throw new NotFoundException('Cannot found email');
    }
    const forgotPasswordToken = await this.otpService.createForgotPasswordToken();
    const data = `${email}-${forgotPasswordToken}`;
    const encryptData = await this.encryptionService.encryptAES(data);
    const link = `${
      process.env.FRONT_END_URL + '/api/verify-forgot-password/?token='
    }${encryptData}`;
    const dataToSend = {
      email,
      link,
    };
    await this.emailService.sendEmail(dataToSend);

    return { url: link, message: 'Success' };
  }

  async verifyForgotPassword(
    token: string,
  ): Promise<VerifyForgotPasswordResponse> {
    const decryptedToken = await this.encryptionService.decryptAES(token);
    const [email, forgotPasswordToken] = decryptedToken.split('-');
    const user = await this.userService.findOneOrFail({
      email,
    });
    if (!user) {
      throw new NotFoundException('Cannot found email');
    }
    const check = await this.otpService.verifyForgotPasswordToken(
      forgotPasswordToken,
    );
    if (!check) {
      throw new BadRequestException('Invalid token');
    }
    return {
      message: 'Success!',
    };
  }

  register(body: RegisterRequest): Promise<RegisterResponse> {
    return this.userService.saveRegister(body);
  }

  async login({ email, password }): Promise<TokenResponse> {
    const user = await this.userService.findOne({ email });
    if (!user) {
      throw new NotFoundException('Cannot found email!');
    }

    const isValid = this.encryptionService.isValidPassword(
      password,
      user.salt,
      user.password,
    );
    if (!isValid) {
      throw new BadRequestException('Password not match!');
    }
    user.jti = uuidv4();
    await this.userService.update(user.id, user);
    const accessToken = await this.tokenService.generateToken(user);
    const refreshToken = await this.tokenService.generateRefreshToken(user);

    return {
      accessToken,
      refreshToken,
    };
  }

  async refreshToken(refreshToken: string): Promise<TokenResponse> {
    const decoded: any = await this.tokenService.verifyRefreshToken(
      refreshToken,
    );
    const user = await this.userService.findOneOrFail(decoded.id);

    const accessToken = await this.tokenService.generateToken(user);
    const refresh = await this.tokenService.generateRefreshToken(user);
    await this.userService.update(user.id, user);

    return {
      accessToken,
      refreshToken: refresh,
    };
  }
}
