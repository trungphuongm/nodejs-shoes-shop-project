import { IsOptional } from 'class-validator';

export class CategoryGet {
  @IsOptional()
  idCategoryParent: number;

  @IsOptional()
  slugCategoryParent: string;
}
