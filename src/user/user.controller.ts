import {
  Controller,
  Get,
  UseInterceptors,
  UseGuards,
  Body,
  HttpCode,
  InternalServerErrorException,
  Patch,
  Param,
  BadRequestException,
  Post,
  NotFoundException,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { ExceptionInterceptor } from '../common/interceptors/exception.interceptor';
import { AuthGuard } from '../common/guards/auth.guard';
import { UserDecorator } from '../common/decorators/user-decorator';
import { UpdatePasswordRequest, UserRequest } from './dto';
import { UserUpdate } from './dto/update-user.dto';
import { Not } from 'typeorm';

@Controller('users')
@UseGuards(AuthGuard)
@UseInterceptors(ExceptionInterceptor)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('profile')
  getProfile(@UserDecorator() user): Promise<User> {
    return this.userService.getById(user.id);
  }

  @Post()
  @HttpCode(201)
  async createUser(@Body() user: UserRequest): Promise<any> {
    return this.userService.create(user);
  }

  @Patch(':id')
  @HttpCode(204)
  async updateUser(
    @Param('id') id: number,
    @Body() userUpdate: UserUpdate,
  ): Promise<boolean> {
    const isUserExist = await this.userService.findOne(id);
    if (!isUserExist) {
      throw new NotFoundException('User not found');
    }
    const isEmailExist = await this.userService.findOne({
      email: userUpdate.email,
      id: Not(id),
    });
    if (isEmailExist) {
      throw new BadRequestException('Email is exist!');
    }
    const isUpdated = await this.userService.update(id, userUpdate);
    if (isUpdated.affected < 1) {
      throw new InternalServerErrorException(`cannot update with id ${id}!`);
    }
    return true;
  }

  @Patch('password')
  @HttpCode(204)
  async updatePassword(
    @UserDecorator() user,
    @Body() body: UpdatePasswordRequest,
  ): Promise<boolean> {
    const updated = await this.userService.updatePassword(user.id, body);
    if (updated.affected < 1) {
      throw new InternalServerErrorException(
        `cannot update with id ${user.id}!`,
      );
    }
    return true;
  }
}
