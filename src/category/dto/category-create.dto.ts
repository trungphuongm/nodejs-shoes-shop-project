import { IsNotEmpty } from 'class-validator';
import { CategoryRequest } from './category-request.dto';

export class CategoryCreate extends CategoryRequest {
  @IsNotEmpty()
  idCategoryParent: number;
}
