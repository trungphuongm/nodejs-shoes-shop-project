import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Repository,
  UpdateResult,
  DeleteResult,
  createQueryBuilder,
} from 'typeorm';
import { User } from './user.entity';
import { UserRequest, UserUpdate } from './dto';
import { EncryptionService } from '../common/services/encryption.service';
import { Role } from '../role/role.entity';
import { RegisterRequest } from '../auth/dto';
import { RegisterResponse } from '../auth/auth.interface';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly encryptionService: EncryptionService,
  ) {}

  findAndCount({
    page,
    limit,
    status,
    email,
    mobile,
  }): Promise<[User[], number]> {
    const offset = page > 0 ? (page - 1) * limit : 0;
    let query = createQueryBuilder(User)
      .innerJoinAndSelect(Role, 'Role', 'Role.id = User.roleId')
      .select([
        'User.id as id',
        '"email"',
        '"fullName"',
        'mobile',
        'address',
        'Role.name as "roleName"',
        'Role.access as access',
        'status',
        'User.createdAt as "createdAt"',
        'User.updatedAt as "updatedAt"',
      ]);
    if (status) {
      query = query.andWhere(`"User"."userId" = :status`, { status });
    }
    if (email) {
      query = query.andWhere(`"User"."email" LIKE :email`, {
        email: '%' + email + '%',
      });
    }
    if (mobile) {
      query = query.andWhere(`"User"."mobile" LIKE :mobile`, {
        mobile: '%' + mobile + '%',
      });
    }

    return Promise.all([
      query
        .orderBy(`"User"."id"`, 'DESC')
        .offset(offset)
        .limit(limit)
        .getRawMany(),
      query.getCount(),
    ]);
  }

  findOne(condition, options = {}): Promise<User> {
    return this.userRepository.findOne(condition, options);
  }

  findOneOrFail(options): Promise<User> {
    return this.userRepository.findOneOrFail(options);
  }

  getById(id: number): Promise<User> {
    return createQueryBuilder(User)
      .innerJoinAndSelect(Role, 'Role', 'Role.id = User.roleId')
      .select([
        'User.id as id',
        'User.email as email',
        '"firstName"',
        '"lastName"',
        'mobile',
        'address',
        'Role.id as "roleId"',
        'Role.name as "roleName"',
        'Role.access as access',
        'status',
        'User.createdAt as "createdAt"',
        'User.updatedAt as "updatedAt"',
      ])
      .where({ id })
      .getRawOne();
  }

  async create(data: UserRequest): Promise<User> {
    const findUser = await this.userRepository.findOne({ email: data.email });
    if (findUser) {
      throw new BadRequestException('Email is exist!');
    }
    const salt = this.encryptionService.getSalt();
    const password = this.encryptionService.encryptPassword(
      data.password,
      salt,
    );

    const user = new User();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.email = data.email;
    user.mobile = data.mobile;
    user.password = password;
    user.salt = salt;
    user.status = data.status;
    user.roleId = data.roleId;

    return this.userRepository.save(user);
  }

  async saveRegister(data: RegisterRequest): Promise<RegisterResponse> {
    const findUser = await this.findOne({ email: data.email });
    if (findUser) {
      throw new BadRequestException('Email is exist!');
    }
    const salt = this.encryptionService.getSalt();
    const password = this.encryptionService.encryptPassword(
      data.password,
      salt,
    );
    const user = new User();
    user.firstName = data.firstName;
    user.lastName = data.lastName;
    user.password = password;
    user.email = data.email;
    user.salt = salt;

    await this.userRepository.save(user);

    return {
      id: user.id,
      message: 'Success!',
    };
  }

  update(id: number, data: UserUpdate): Promise<UpdateResult> {
    const user = new User();
    if (data.email) user.email = data.email;
    if (data.firstName) user.firstName = data.firstName;
    if (data.lastName) user.lastName = data.lastName;
    if (data.mobile) user.mobile = data.mobile;
    if (data.jti) user.jti = data.jti;
    if (data.gender) user.gender = data.gender;
    if (data.birthday) user.birthday = data.birthday;

    return this.userRepository.update(id, user);
  }

  async updatePassword(
    id: number,
    { oldPassword, newPassword },
  ): Promise<UpdateResult> {
    const currentUser = await this.userRepository.findOne(id);
    if (!currentUser) {
      throw new NotFoundException('cannot found user!');
    }
    const hashOldPassword = this.encryptionService.encryptPassword(
      oldPassword,
      currentUser.salt,
    );
    if (hashOldPassword !== currentUser.password) {
      throw new BadRequestException('Password not match!');
    }

    const salt = this.encryptionService.getSalt();
    const password = this.encryptionService.encryptPassword(newPassword, salt);

    const user = new User();
    user.salt = salt;
    user.password = password;

    return this.userRepository.update(id, user);
  }

  async delete(id: number): Promise<DeleteResult> {
    return this.userRepository.delete(id);
  }
}
