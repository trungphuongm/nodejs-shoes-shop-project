import {
  IsOptional,
  IsIn,
  IsNotEmpty,
  IsEmail,
  Validate,
  IsNumber,
} from 'class-validator';
import { ACCOUNT_ACTIVE, ACCOUNT_INACTIVE } from '../../common/constant';
import { IsValidMobile } from '../../common/validators/is-valid-mobile';

export class UserRequest {
  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Validate(IsValidMobile)
  mobile: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  @IsIn([ACCOUNT_ACTIVE, ACCOUNT_INACTIVE])
  status: string;

  @IsOptional()
  address: string;

  @IsNumber()
  roleId: number;

  @IsOptional()
  jti: string;
}
