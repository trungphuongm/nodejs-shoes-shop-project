import { Role } from './role.entity';

export interface SearchPaginationResponse {
  data: Role[];
  totalCount: number;
}

export interface CreateUserResponse {
  id: string;
}

export interface RoleResponse {
  id: number;
  name: string;
  access: string;
}
