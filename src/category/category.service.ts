import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TreeRepository } from 'typeorm';
import { Category } from './category.entity';
import { CategoryGet } from './dto/category-get.dto';
import { CategoryCreate } from './dto/category-create.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: TreeRepository<Category>,
  ) {}

  async createCategory(categoryCreate: CategoryCreate): Promise<Category> {
    const parentExist = await this.categoryRepository.findOne({
      id: categoryCreate.idCategoryParent,
    });
    if (!parentExist) {
      throw new NotFoundException('Parent category is not exist');
    }
    const categoryParent = new Category();
    categoryParent.id = categoryCreate.idCategoryParent;

    const category = new Category();
    if (categoryCreate.name) category.name = categoryCreate.name.trim();
    if (categoryCreate.slug)
      category.slug = categoryCreate.slug
        .trim()
        .toLowerCase()
        .replace(' ', '-');
    else {
      category.slug = categoryCreate.name
        .trim()
        .toLowerCase()
        .replace(' ', '-');
    }
    if (categoryCreate.description) {
      category.description = categoryCreate.description;
    }
    category.parentCategory = categoryParent;
    return this.categoryRepository.save(category);
  }

  async getTreeCategory(categoryGet: CategoryGet): Promise<Category> {
    let where = {};
    const parentCategory = new Category();
    if (categoryGet.idCategoryParent) {
      where = {
        id: categoryGet.idCategoryParent,
      };
      parentCategory.id = +categoryGet.idCategoryParent;
    }
    if (categoryGet.slugCategoryParent) {
      where = { ...where, slug: categoryGet.slugCategoryParent };
      parentCategory.slug = categoryGet.slugCategoryParent;
    }
    const parentCategoryExist = await this.categoryRepository.findOne(where);
    if (!parentCategoryExist) {
      throw new NotFoundException('Parent category is not exist');
    }
    return this.categoryRepository.findDescendantsTree(parentCategoryExist);
  }
  getAllCategory(): Promise<Category[]> {
    return this.categoryRepository.findTrees();
  }
}
